<?php

namespace Test\Unit;

class DataProvider
{
    // $Subject, $Message, $AttachmentPath, $AttachmentName
    public function dataSmtp(): array
    {
        $time = time();

        $directory = realpath(__DIR__);
        $html = $directory . DIRECTORY_SEPARATOR . "smtp.html";
        $mjml = $directory . DIRECTORY_SEPARATOR . "smtp.mjml";

        return array(
            array(
                "Test message from SMTP - " . $time,
                (is_file($html) ? @file_get_contents($html) : ""),
                (is_file($mjml) ? $mjml : ""),
                "sample-" . $time . ".txt"
            )
        );
    }

    // $Subject, $Message, $AttachmentPath, $AttachmentName
    public function dataSendInBlue(): array
    {
        $time = time();

        $directory = realpath(__DIR__);
        $html = $directory . DIRECTORY_SEPARATOR . "sendinblue.html";
        $mjml = $directory . DIRECTORY_SEPARATOR . "sendinblue.mjml";

        return array(
            array(
                "Test message from SendInBlue - " . $time,
                (is_file($html) ? @file_get_contents($html) : ""),
                (is_file($mjml) ? $mjml : ""),
                "sample-" . $time . ".txt"
            )
        );
    }
}
