<?php

namespace Test\Unit;

use Samy\Email\SendInBlue;

class SendInBlueTest extends AssertEmail
{
    /**
     * @dataProvider \Test\Unit\DataProvider::dataSendInBlue
     */
    public function testSendInBlue($Subject, $Message, $AttachmentPath, $AttachmentName): void
    {
        $email = new SendInBlue(SENDINBLUE_APIKEY);

        $this->assertEmail(
            $email,
            (defined("SENDINBLUE_ADDRESS") ? SENDINBLUE_ADDRESS : ""),
            (defined("SENDINBLUE_DISPLAY") ? SENDINBLUE_DISPLAY : ""),
            (defined("RECIPIENT_EMAIL") ? RECIPIENT_EMAIL : ""),
            (defined("RECIPIENT_NAME") ? RECIPIENT_NAME : ""),
            (defined("CC_EMAIL") ? CC_EMAIL : ""),
            (defined("CC_NAME") ? CC_NAME : ""),
            (defined("BCC_EMAIL") ? BCC_EMAIL : ""),
            (defined("BCC_NAME") ? BCC_NAME : ""),
            $Subject,
            $Message,
            $AttachmentPath,
            $AttachmentName
        );
    }
}
