<?php

namespace Test\Unit;

use PHPUnit\Framework\TestCase;
use Samy\Email\AbstractEmail;
use Samy\Email\EmailInterface;

abstract class AssertEmail extends TestCase
{
    protected function assertEmail(
        EmailInterface &$EmailInterface,
        string $SenderAddress,
        string $SenderDisplay,
        string $RecipientEmail,
        string $RecipientName,
        string $CcEmail,
        string $CcName,
        string $BccEmail,
        string $BccName,
        string $Subject,
        string $Message,
        string $AttachmentPath,
        string $AttachmentName
    ): void {
        $this->assertInstanceOf(
            AbstractEmail::class,
            $EmailInterface
                ->withSenderAddress($SenderAddress)
                ->withSenderDisplay($SenderDisplay)
                ->withRecipient($RecipientEmail, $RecipientName)
                ->withReplyTo($SenderAddress, $SenderDisplay)
                ->withAttachment($AttachmentPath, $AttachmentName)
        );


        $this->assertSame($SenderAddress, $EmailInterface->getSenderAddress($SenderAddress));
        $this->assertSame($SenderDisplay, $EmailInterface->getSenderDisplay($SenderDisplay));

        $this->assertSame(array($RecipientEmail => $RecipientName), $EmailInterface->getRecipients());
        $this->assertSame(array($SenderAddress => $SenderDisplay), $EmailInterface->getRepliesTo());
        $this->assertSame(array($AttachmentPath => $AttachmentName), $EmailInterface->getAttachments());


        if (($CcEmail != "") && ($CcName != "")) {
            $this->assertInstanceOf(AbstractEmail::class, $EmailInterface->withCc($CcEmail, $CcName));

            $this->assertSame(
                array($CcEmail => $CcName),
                $EmailInterface->getAllCc()
            );
        }

        if (($BccEmail != "") && ($BccName != "")) {
            $this->assertInstanceOf(AbstractEmail::class, $EmailInterface->withBcc($BccEmail, $BccName));

            $this->assertSame(
                array($BccEmail => $BccName),
                $EmailInterface->getAllBcc()
            );
        }


        $this->assertInstanceOf(AbstractEmail::class, $EmailInterface->send($Subject, $Message));

        $this->assertSame("", $EmailInterface->getLastError());
    }
}
