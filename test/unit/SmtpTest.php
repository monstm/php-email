<?php

namespace Test\Unit;

use Samy\Email\Smtp;

class SmtpTest extends AssertEmail
{
    /**
     * @dataProvider \Test\Unit\DataProvider::dataSmtp
     */
    public function testSmtp($Subject, $Message, $AttachmentPath, $AttachmentName): void
    {
        $email = new Smtp(SMTP_HOST, SMTP_USERNAME, SMTP_PASSWORD, intval(SMTP_PORT));

        $this->assertEmail(
            $email,
            (defined("SMTP_ADDRESS") ? SMTP_ADDRESS : ""),
            (defined("SMTP_DISPLAY") ? SMTP_DISPLAY : ""),
            (defined("RECIPIENT_EMAIL") ? RECIPIENT_EMAIL : ""),
            (defined("RECIPIENT_NAME") ? RECIPIENT_NAME : ""),
            (defined("CC_EMAIL") ? CC_EMAIL : ""),
            (defined("CC_NAME") ? CC_NAME : ""),
            (defined("BCC_EMAIL") ? BCC_EMAIL : ""),
            (defined("BCC_NAME") ? BCC_NAME : ""),
            $Subject,
            $Message,
            $AttachmentPath,
            $AttachmentName
        );
    }
}
