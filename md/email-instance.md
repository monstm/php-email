# Email Instance

Simple Email Implementation.

---

## SMTP

Implementation SMTP instance.

```php
$email = new Smtp($host, $username, $password, $port);
```

### getDebugLevel

Retrieve debug level.

```php
$debug_level = $email->getDebugLevel();
```

### withDebugLevel

Return an instance with the specific debug level.

```php
$email = $email->withDebugLevel($debug_level);
```

| debug_level | Description                                                            |
|:-----------:|:-----------------------------------------------------------------------|
| 0           | no output                                                              |
| 1           | show client -> server messages                                         |
| 2           | show client -> server and server -> client messages                    |
| 3           | show connection status, client -> server and server -> client messages |
| 4           | show all messages                                                      |

---

## SendInBlue

Implementation SendInBlue instance.

```php
$email = new SendInBlue($api_key);
```
