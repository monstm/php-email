# Email Interface

Describes Email interface.

---

## getSenderAddress

Retrieve sender email address.

```php
$sender_address = $email->getSenderAddress();
```

---

## withSenderAddress

Return an instance with the specific sender email address.

```php
$email = $email->withSenderAddress($sender_address);
```

---

## getSenderDisplay

Retrieve sender display name.

```php
$sender_display = $email->getSenderDisplay();
```

---

## withSenderDisplay

Return an instance with the specific sender display name.

```php
$email = $email->withSenderDisplay($sender_display);
```

---

## withoutSenderDisplay

Return an instance without the specific sender display name.

```php
$email = $email->withoutSenderDisplay();
```

---

## getRecipients

Retrieve all recipients.

```php
$recipients = $email->getRecipients();
```

---

## withRecipient

Return an instance with the specified recipient appended with the given value.

```php
$email = $email->withRecipient($recipient_email, $recipient_name = "");
```

---

## withoutRecipient

Return an instance without the specified recipient.

```php
$email = $email->withoutRecipient($recipient_email);
```

---

## withoutRecipients

Return an instance without any recipients.

```php
$email = $email->withoutRecipients();
```

---

## getRepliesTo

Retrieve all reply-to.

```php
$replies = $email->getRepliesTo();
```

---

## withReplyTo

Return an instance with the specified reply-to appended with the given value.

```php
$email = $email->withReplyTo($reply_email, $reply_name = "");
```

---

## withoutReplyTo

Return an instance without the specified reply-to.

```php
$email = $email->withoutReplyTo($reply_email);
```

---

## withoutRepliesTo

Return an instance without any reply-to.

```php
$email = $email->withoutRepliesTo($reply_email);
```

---

## getAllCc

Retrieve all CC.

```php
$all_cc = $email->getAllCc();
```

---

## withCc

Return an instance with the specified CC appended with the given value.

```php
$email = $email->withCc($cc_email, $cc_name = "");
```

---

## withoutCc

Return an instance without the specified CC.

```php
$email = $email->withoutCc($cc_email);
```

---

## withoutAllCc

Return an instance without any CC.

```php
$email = $email->withoutAllCc();
```

---

## getAllBcc

Retrieve all BCC.

```php
$all_bcc = $email->getAllBcc();
```

---

## withBcc

Return an instance with the specified BCC appended with the given value.

```php
$email = $email->withBcc($bcc_email, $bcc_name = "");
```

---

## withoutBcc

Return an instance without the specified BCC.

```php
$email = $email->withoutBcc($bcc_email);
```

---

## withoutAllBcc

Return an instance without any BCC.

```php
$email = $email->withoutAllBcc();
```

---

## getAttachments

Retrieve all attachments.

```php
$attachments = $email->getAttachments();
```

---

## withAttachment

Return an instance with the specified attachment appended with the given value.

```php
$email = $email->withAttachment($attachment_uri, $attachment_name = "");
```

---

## withoutAttachment

Return an instance without the specified attachment.

```php
$email = $email->withoutAttachment($attachment_uri);
```

---

## withoutAttachments

Return an instance without any attachment.

```php
$email = $email->withoutAttachments();
```

---

## send

Send email.

```php
$email = $email->send($subject, $message);
```

---

## getLastError

Retrieve last error.

```php
$last_error = $email->getLastError();
```
