<?php

namespace Samy\Email;

/**
 * Describes Email interface.
 */
interface EmailInterface
{
    /**
     * Retrieve sender email address.
     *
     * @return string
     */
    public function getSenderAddress(): string;

    /**
     * Return an instance with the specific sender email address.
     *
     * @param[in] string $Email Sender email address
     *
     * @return static
     */
    public function withSenderAddress(string $Email): self;


    /**
     * Retrieve sender display name.
     *
     * @return string
     */
    public function getSenderDisplay(): string;

    /**
     * Return an instance with the specific sender display name.
     *
     * @param[in] string $Name Sender display name
     *
     * @return static
     */
    public function withSenderDisplay(string $Name): self;

    /**
     * Return an instance without the specific sender display name.
     *
     * @return static
     */
    public function withoutSenderDisplay(): self;


    /**
     * Retrieve all recipients.
     *
     * @return array<string, string> all recipients
     */
    public function getRecipients(): array;

    /**
     * Return an instance with the specified recipient appended with the given value.
     *
     * @param[in] string $Email Recipient email address
     * @param[in] string $Name Recipient display name
     *
     * @return static
     */
    public function withRecipient(string $Email, string $Name = ""): self;

    /**
     * Return an instance without the specified recipient.
     *
     * @param[in] string $Email Recipient email address
     *
     * @return static
     */
    public function withoutRecipient(string $Email): self;

    /**
     * Return an instance without any recipients.
     *
     * @return static
     */
    public function withoutRecipients(): self;


    /**
     * Retrieve all reply-to.
     *
     * @return array<string, string> all reply-to
     */
    public function getRepliesTo(): array;

    /**
     * Return an instance with the specified reply-to appended with the given value.
     *
     * @param[in] string $Email Reply-To email address
     * @param[in] string $Name Reply-To display name
     *
     * @return static
     */
    public function withReplyTo(string $Email, string $Name = ""): self;

    /**
     * Return an instance without the specified reply-to.
     *
     * @param[in] string $Email Reply-To email address
     *
     * @return static
     */
    public function withoutReplyTo(string $Email): self;

    /**
     * Return an instance without any reply-to.
     *
     * @return static
     */
    public function withoutRepliesTo(): self;


    /**
     * Retrieve all CC.
     *
     * @return array<string, string> all CC
     */
    public function getAllCc(): array;

    /**
     * Return an instance with the specified CC appended with the given value.
     *
     * @param[in] string $Email CC email address
     * @param[in] string $Name CC display name
     *
     * @return static
     */
    public function withCc(string $Email, string $Name = ""): self;

    /**
     * Return an instance without the specified CC.
     *
     * @param[in] string $Email CC email address
     *
     * @return static
     */
    public function withoutCc(string $Email): self;

    /**
     * Return an instance without any CC.
     *
     * @return static
     */
    public function withoutAllCc(): self;


    /**
     * Retrieve all BCC.
     *
     * @return array<string, string> all BCC
     */
    public function getAllBcc(): array;

    /**
     * Return an instance with the specified BCC appended with the given value.
     *
     * @param[in] string $Email BCC email address
     * @param[in] string $Name BCC display name
     *
     * @return static
     */
    public function withBcc(string $Email, string $Name = ""): self;

    /**
     * Return an instance without the specified BCC.
     *
     * @param[in] string $Email BCC email address
     *
     * @return static
     */
    public function withoutBcc(string $Email): self;

    /**
     * Return an instance without any BCC.
     *
     * @return static
     */
    public function withoutAllBcc(): self;


    /**
     * Retrieve all attachments.
     *
     * @return array<string, string> all attachments
     */
    public function getAttachments(): array;

    /**
     * Return an instance with the specified attachment appended with the given value.
     *
     * @param[in] string $Uri File uri
     * @param[in] string $Name File name
     *
     * @return static
     */
    public function withAttachment(string $Uri, string $Name = ""): self;

    /**
     * Return an instance without the specified attachment.
     *
     * @param[in] string $Uri File uri
     *
     * @return static
     */
    public function withoutAttachment(string $Uri): self;

    /**
     * Return an instance without any attachment.
     *
     * @return static
     */
    public function withoutAttachments(): self;


    /**
     * Send email.
     *
     * @param[in] string $Subject Email subject
     * @param[in] string $Message Email message
     *
     * @return static
     */
    public function send(string $Subject, string $Message): self;


    /**
     * Retrieve last error.
     *
     * @return string
     */
    public function getLastError(): string;
}
