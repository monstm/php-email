<?php

namespace Samy\Email;

use Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Samy\Log\Syslog;

/**
 * Simple SMTP implementation.
 */
class Smtp extends AbstractEmail
{
    private $scheme = "";
    private $host = "";
    private $username = "";
    private $password = "";
    private $port = 0;

    private $debug = 0;


    /**
     * Smtp construction.
     *
     * @param[in] string $Host Smtp host
     * @param[in] string $Username Smtp Username
     * @param[in] string $Password Smtp Password
     * @param[in] int $Port Smtp Port
     *
     * @return void
     */
    public function __construct(string $Host, string $Username, string $Password, int $Port)
    {
        $parse_url = parse_url($Host);

        if (isset($parse_url["scheme"])) {
            $this->scheme = $parse_url["scheme"];
        }

        $this->host = (isset($parse_url["host"]) ?
            $parse_url["host"] : $Host
        );

        $this->username = $Username;
        $this->password = $Password;
        $this->port = $Port;

        if (filter_var($Username, FILTER_VALIDATE_EMAIL)) {
            $this->withSenderAddress($Username);
        }
    }


    /**
     * Send email.
     *
     * @param[in] string $Subject Email subject
     * @param[in] string $Message Email message
     *
     * @return static
     */
    public function send(string $Subject, string $Message): self
    {
        $log = new Syslog();

        try {
            $phpmailer = new PHPMailer(true);

            $phpmailer->SMTPDebug = $this->debug;
            $phpmailer->isSMTP();
            $phpmailer->SMTPAuth = true;

            $phpmailer->SMTPSecure = $this->scheme;
            $phpmailer->Host = $this->host;
            $phpmailer->Username = $this->username;
            $phpmailer->Password = $this->password;
            $phpmailer->Port = $this->port;

            $phpmailer->setFrom(
                $this->getSenderAddress(),
                $this->getSenderDisplay()
            );

            foreach ($this->getRecipients() as $email => $name) {
                $phpmailer->addAddress($email, $name);
            }

            foreach ($this->getRepliesTo() as $email => $name) {
                $phpmailer->addReplyTo($email, $name);
            }

            foreach ($this->getAllCc() as $email => $name) {
                $phpmailer->addCC($email, $name);
            }

            foreach ($this->getAllBcc() as $email => $name) {
                $phpmailer->addBCC($email, $name);
            }

            foreach ($this->getAttachments() as $uri => $name) {
                if (is_file($uri)) {
                    $phpmailer->addAttachment($uri, $name);
                } else {
                    $log->backtrace($uri . " is not filename");
                }
            }

            $phpmailer->isHTML(true);
            $phpmailer->Subject = $Subject;
            $phpmailer->Body = $Message;
            $phpmailer->AltBody = strip_tags($Message);

            $phpmailer->send();

            $this->clearError();
        } catch (Exception $exception) {
            $log->exception($exception);

            $this->setLastError($exception->getMessage());
        }

        return $this;
    }


    /**
     * Retrieve debug level.
     *
     * @return int
     */
    public function getDebugLevel(): int
    {
        return $this->debug;
    }

    /**
     * Return an instance with the specific debug level.
     *
     * option:
     * 0 - no output.
     * 1 - show client -> server messages.
     * 2 - show client -> server and server -> client messages.
     * 3 - show connection status, client -> server and server -> client messages.
     * 4 - show all messages.
     *
     * @param[in] int $Level Debug level
     *
     * @return static
     */
    public function withDebugLevel(int $Level): self
    {
        $this->debug = (in_array($Level, array(0, 1, 2, 3, 4)) ? $Level : 0);

        return $this;
    }
}
