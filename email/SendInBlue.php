<?php

namespace Samy\Email;

use Exception;
use Samy\Log\Syslog;
use Samy\Psr18\Client;
use Samy\Psr7\Request;
use Samy\Psr7\Stream;
use Samy\Psr7\Uri;

/**
 * Simple SendInBlue implementation.
 */
class SendInBlue extends AbstractEmail
{
    private const NAME_LIMIT = 70; // limit characters

    private $api_key = "";


    /**
     * SendInBlue construction.
     *
     * @param[in] string $ApiKey SendInBlue api key v3
     *
     * @return void
     */
    public function __construct(string $ApiKey)
    {
        $this->api_key = $ApiKey;
    }


    /**
     * Send email.
     *
     * @param[in] string $Subject Email subject
     * @param[in] string $Message Email message
     *
     * @return static
     */
    public function send(string $Subject, string $Message): self
    {
        $log = new Syslog();

        try {
            $data = array(
                "subject" => $Subject,
                "htmlContent" => $Message,
                "textContent" => strip_tags($Message)
            );

            $this
                ->fillSenderObject($data)
                ->fillReplyObject($data)
                ->fillEmailObject($data, "to", $this->getRecipients())
                ->fillEmailObject($data, "cc", $this->getAllCc())
                ->fillEmailObject($data, "bcc", $this->getAllBcc())
                ->fillAttachmentObject($data);

            $content = json_encode($data);

            $stream = new Stream();
            $stream
                ->withTemp()
                ->write(is_string($content) ? $content : "");

            $uri = new Uri();
            $uri->parseUrl("https://api.sendinblue.com/v3/smtp/email");

            $request = new Request();
            $request
                ->withMethod("POST")
                ->withHeader("Accept", "application/json")
                ->withHeader("Content-Type", "application/json")
                ->withHeader("api-key", $this->api_key)
                ->withHeader("Content-Length", strval($stream->getSize()))
                ->withBody($stream)
                ->withUri($uri);

            $client = new Client();
            $response = $client->sendRequest($request);

            if ($response->getStatusCode() == 201) {
                $this->clearError();
            } else {
                $json = @json_decode($response->getBody()->getContents(), true);

                $error = ($json ?
                    (isset($json["code"]) ? $json["code"] . " - " : "") .
                    ($json["message"] ?? "") :
                    json_last_error_msg()
                );

                $log->backtrace($error);
                $this->setLastError($error);
            }
        } catch (Exception $exception) {
            $log->exception($exception);

            $this->setLastError($exception->getMessage());
        }


        return $this;
    }


    /**
     * Fill sender object.
     *
     * @param[in,out] array $Result Request object
     *
     * @return static
     */
    private function fillSenderObject(array &$Result): self
    {
        $buffer = array();

        $email = $this->getSenderAddress();
        if ($email != "") {
            $buffer["email"] = $email;

            $name = $this->getSenderDisplay();
            if ($name != "") {
                $buffer["name"] = substr($name, 0, self::NAME_LIMIT);
            }
        }

        if (count($buffer) > 0) {
            $Result["sender"] = $buffer;
        }

        return $this;
    }

    /**
     * Fill reply object.
     *
     * @param[in,out] array $Result Request object
     *
     * @return static
     */
    private function fillReplyObject(array &$Result): self
    {
        $email = $this->getSenderAddress();
        $name = $this->getSenderDisplay();

        foreach ($this->getRepliesTo() as $reply_email => $reply_name) {
            $email = $reply_email;
            $name = $reply_name;
        }

        $buffer = array();
        if ($email != "") {
            $buffer["email"] = $email;

            if ($name != "") {
                $buffer["name"] = substr($name, 0, self::NAME_LIMIT);
            }
        }

        if (count($buffer) > 0) {
            $Result["replyTo"] = $buffer;
        }

        return $this;
    }

    /**
     * Fill email object.
     *
     * @param[in,out] array $Result Request object
     * @param[in] string $Key Request key
     * @param[in] array $Data Email object
     *
     * @return static
     */
    private function fillEmailObject(array &$Result, string $Key, array $Data): self
    {
        $buffer = array();

        foreach ($Data as $email => $name) {
            $temp = array();

            if ($email != "") {
                $temp["email"] = $email;

                if ($name != "") {
                    $temp["name"] = substr($name, 0, self::NAME_LIMIT);
                }

                array_push($buffer, $temp);
            }
        }

        if (count($buffer) > 0) {
            $Result[$Key] = $buffer;
        }

        return $this;
    }

    /**
     * Fill attachment object.
     *
     * @param[in,out] array $Result Request object
     *
     * @return static
     */
    private function fillAttachmentObject(array &$Result): self
    {
        $log = new Syslog();

        $buffer = array();

        foreach ($this->getAttachments() as $uri => $name) {
            if ($name != "") {
                if (is_file($uri)) {
                    $content = @file_get_contents($uri);

                    array_push($buffer, array(
                        "content" => @base64_encode(is_string($content) ? $content : ""),
                        "name" => substr($name, 0, self::NAME_LIMIT)
                    ));
                } elseif (filter_var($uri, FILTER_VALIDATE_URL)) {
                    array_push($buffer, array(
                        "url" => $uri,
                        "name" => substr($name, 0, self::NAME_LIMIT)
                    ));
                } else {
                    $log->backtrace("unsupported for uri " . $uri);
                }
            }
        }

        if (count($buffer) > 0) {
            $Result["attachment"] = $buffer;
        }

        return $this;
    }
}
