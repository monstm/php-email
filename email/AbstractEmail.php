<?php

namespace Samy\Email;

/**
 * This is a simple Mailer implementation that other Mailer can inherit from.
 */
abstract class AbstractEmail implements EmailInterface
{
    private $sender_address = "";
    private $sender_display = "";

    private $recipient = array();
    private $reply = array();
    private $cc = array();
    private $bcc = array();
    private $attachment = array();

    private $last_error = "";


    /**
     * Retrieve sender email address.
     *
     * @return string
     */
    public function getSenderAddress(): string
    {
        return $this->sender_address;
    }

    /**
     * Return an instance with the specific sender email address.
     *
     * @param[in] string $Email Sender email address
     *
     * @return static
     */
    public function withSenderAddress(string $Email): self
    {
        if (filter_var($Email, FILTER_VALIDATE_EMAIL)) {
            $this->sender_address = $Email;
        }

        return $this;
    }


    /**
     * Retrieve sender display name.
     *
     * @return string
     */
    public function getSenderDisplay(): string
    {
        return $this->sender_display;
    }

    /**
     * Return an instance with the specific sender display name.
     *
     * @param[in] string $Name Sender display name
     *
     * @return static
     */
    public function withSenderDisplay(string $Name): self
    {
        $this->sender_display = trim($Name);

        return $this;
    }

    /**
     * Return an instance without the specific sender display name.
     *
     * @return static
     */
    public function withoutSenderDisplay(): self
    {
        $this->sender_display = "";

        return $this;
    }


    /**
     * Retrieve all recipients.
     *
     * @return array<string, string> all recipients
     */
    public function getRecipients(): array
    {
        return $this->recipient;
    }

    /**
     * Return an instance with the specified recipient appended with the given value.
     *
     * @param[in] string $Email Recipient email address
     * @param[in] string $Name Recipient display name
     *
     * @return static
     */
    public function withRecipient(string $Email, string $Name = ""): self
    {
        $key = strtolower(trim($Email));

        if (filter_var($key, FILTER_VALIDATE_EMAIL)) {
            $this->recipient[$key] = trim($Name);
        }

        return $this;
    }

    /**
     * Return an instance without the specified recipient.
     *
     * @param[in] string $Email Recipient email address
     *
     * @return static
     */
    public function withoutRecipient(string $Email): self
    {
        $key = strtolower(trim($Email));

        if (isset($this->recipient[$key])) {
            unset($this->recipient[$key]);
        }

        return $this;
    }

    /**
     * Return an instance without any recipients.
     *
     * @return static
     */
    public function withoutRecipients(): self
    {
        $this->recipient = array();

        return $this;
    }


    /**
     * Retrieve all reply-to.
     *
     * @return array<string, string> all reply-to
     */
    public function getRepliesTo(): array
    {
        return $this->reply;
    }

    /**
     * Return an instance with the specified reply-to appended with the given value.
     *
     * @param[in] string $Email Reply-To email address
     * @param[in] string $Name Reply-To display name
     *
     * @return static
     */
    public function withReplyTo(string $Email, string $Name = ""): self
    {
        $key = strtolower(trim($Email));

        if (filter_var($key, FILTER_VALIDATE_EMAIL)) {
            $this->reply[$key] = trim($Name);
        }

        return $this;
    }

    /**
     * Return an instance without the specified reply-to.
     *
     * @param[in] string $Email Reply-To email address
     *
     * @return static
     */
    public function withoutReplyTo(string $Email): self
    {
        $key = strtolower(trim($Email));

        if (isset($this->reply[$key])) {
            unset($this->reply[$key]);
        }

        return $this;
    }

    /**
     * Return an instance without any reply-to.
     *
     * @return static
     */
    public function withoutRepliesTo(): self
    {
        $this->reply = array();

        return $this;
    }


    /**
     * Retrieve all CC.
     *
     * @return array<string, string> all CC
     */
    public function getAllCc(): array
    {
        return $this->cc;
    }

    /**
     * Return an instance with the specified CC appended with the given value.
     *
     * @param[in] string $Email CC email address
     * @param[in] string $Name CC display name
     *
     * @return static
     */
    public function withCc(string $Email, string $Name = ""): self
    {
        $key = strtolower(trim($Email));

        if (filter_var($key, FILTER_VALIDATE_EMAIL)) {
            $this->cc[$key] = trim($Name);
        }

        return $this;
    }

    /**
     * Return an instance without the specified CC.
     *
     * @param[in] string $Email CC email address
     *
     * @return static
     */
    public function withoutCc(string $Email): self
    {
        $key = strtolower(trim($Email));

        if (isset($this->cc[$key])) {
            unset($this->cc[$key]);
        }

        return $this;
    }

    /**
     * Return an instance without any CC.
     *
     * @return static
     */
    public function withoutAllCc(): self
    {
        $this->cc = array();

        return $this;
    }


    /**
     * Retrieve all BCC.
     *
     * @return array<string, string> all BCC
     */
    public function getAllBcc(): array
    {
        return $this->bcc;
    }

    /**
     * Return an instance with the specified BCC appended with the given value.
     *
     * @param[in] string $Email BCC email address
     * @param[in] string $Name BCC display name
     *
     * @return static
     */
    public function withBcc(string $Email, string $Name = ""): self
    {
        $key = strtolower(trim($Email));

        if (filter_var($key, FILTER_VALIDATE_EMAIL)) {
            $this->bcc[$key] = trim($Name);
        }

        return $this;
    }

    /**
     * Return an instance without the specified BCC.
     *
     * @param[in] string $Email BCC email address
     *
     * @return static
     */
    public function withoutBcc(string $Email): self
    {
        $key = strtolower(trim($Email));

        if (isset($this->bcc[$key])) {
            unset($this->bcc[$key]);
        }

        return $this;
    }

    /**
     * Return an instance without any BCC.
     *
     * @return static
     */
    public function withoutAllBcc(): self
    {
        $this->bcc = array();

        return $this;
    }


    /**
     * Retrieve all attachments.
     *
     * @return array<string, string> all attachments
     */
    public function getAttachments(): array
    {
        return $this->attachment;
    }

    /**
     * Return an instance with the specified attachment appended with the given value.
     *
     * @param[in] string $Uri File uri
     * @param[in] string $Name File name
     *
     * @return static
     */
    public function withAttachment(string $Uri, string $Name = ""): self
    {
        $this->attachment[$Uri] = trim($Name);

        return $this;
    }

    /**
     * Return an instance without the specified attachment.
     *
     * @param[in] string $Uri File uri
     *
     * @return static
     */
    public function withoutAttachment(string $Uri): self
    {
        if (isset($this->attachment[$Uri])) {
            unset($this->attachment[$Uri]);
        }

        return $this;
    }

    /**
     * Return an instance without any attachment.
     *
     * @return static
     */
    public function withoutAttachments(): self
    {
        $this->attachment = array();

        return $this;
    }


    /**
     * Retrieve last error.
     *
     * @return string
     */
    public function getLastError(): string
    {
        $ret = $this->last_error;
        $this->last_error = "";

        return $ret;
    }

    /**
     * Set error data.
     *
     * @return void
     */
    protected function setLastError(string $Message): void
    {
        $this->last_error = $Message;
    }

    /**
     * Clear error data.
     *
     * @return void
     */
    protected function clearError(): void
    {
        $this->setLastError("");
    }
}
