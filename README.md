# PHP Email

[
	![](https://badgen.net/packagist/v/samy/email/latest)
	![](https://badgen.net/packagist/license/samy/email)
	![](https://badgen.net/packagist/dt/samy/email)
	![](https://badgen.net/packagist/favers/samy/email)
](https://packagist.org/packages/samy/email)

This is a simple way to interact with email protocols.

---

## Instalation

### Composer

Composer is a tool for dependency management in PHP.
It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

```sh
composer require samy/email
```

Please see [Composer](https://getcomposer.org/) for more information.

---

## Support

* Repository: <https://gitlab.com/monstm/php-email>
* Documentations: <https://monstm.gitlab.io/php-email/>
* Annotation: <https://monstm.alwaysdata.net/php-email/>
* Issues: <https://gitlab.com/monstm/php-email/-/issues>
